package kg.adis.testproject;

public class AppConfig {

    public static String PREF_KEY = "CQ4qzCWyMYaoUYOIjfM66aGrfaMHJq1r";
    public static String PREF_NAME = "kg.adis.testproject";

    public static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    public static final String USERNAME_SHARED = "USERNAME_SHARED";
    public static final String EMAIL_SHARED = "EMAIL_SHARED";



    public static String API_URL = "https://e4abd3b0.eu.ngrok.io/api/v1/";

    public class Keys {

        /* USER */
        public static final String USER_NAME = "USER_NAME";
        public static final String USER_EMAIL= "USER_EMAIL";
        public static final String USER_API_TOKEN = "USER_API_TOKEN";
        public static final String USER_API_DATE = "USER_API_DATE";
    }

}
