package kg.adis.testproject.api.model.user;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.Date;

import kg.adis.testproject.AppConfig;
import kg.adis.testproject.util.SecurePreferences;


public class User {

    private Integer id;
    private String email;
    private String token;
    private Long token_expire_date;
    private String username;
    private Context context;
    private static User instance;


    //region getter setter
    public int getId() {
        return this.id;
    }

    public void setId(int _id) {
        this.id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserame() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public Long getTokenExpire() {
        return token_expire_date;
    }

    public Long setTokenExpire(Long token_expire_date) {
        return this.token_expire_date = token_expire_date;
    }

    public User(Context context) {
        SecurePreferences pref = SecurePreferences.getInstance(context);

        this.username = pref.getString(AppConfig.Keys.USER_NAME, "");
        this.email = pref.getString(AppConfig.Keys.USER_EMAIL, "");
        this.token = pref.getString(AppConfig.Keys.USER_API_TOKEN, "");
        this.token_expire_date = Long.valueOf(pref.getString(AppConfig.Keys.USER_API_DATE, "1529368583000"));
        this.context = context;
    }

    public boolean isTokenExpired() {
        return this.getTokenExpire() < new Date().getTime();
    }

    public boolean isTokenValid() {
        return this.getTokenExpire() >= new Date().getTime();
    }

    public User fromJson(JsonElement jsonElement) {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
        return gson.fromJson(jsonElement, User.class);
    }

    public void clearApiDate() {
        SecurePreferences.getInstance(this.context).put(AppConfig.Keys.USER_API_DATE, "1529368583000");
        this.setTokenExpire((long) 1529368583 * 1000);
    }
    public void clear_user_credentials() {
        SecurePreferences.getInstance(this.context).clear();
        this.setTokenExpire((long) 1529368583 * 1000);
    }

    public static User getInstance(Context context) {
        if (instance == null) {
            instance = new User(context);
        }
        return instance;
    }

    public void update(JsonElement data) {
        SecurePreferences pref = SecurePreferences.getInstance(this.context);
        instance = fromJson(data);
        pref.put(AppConfig.Keys.USER_NAME, instance.getUserame());
        pref.put(AppConfig.Keys.USER_EMAIL, instance.getEmail());
        pref.put(AppConfig.Keys.USER_API_TOKEN, instance.getToken());
        pref.put(AppConfig.Keys.USER_API_DATE, instance.getTokenExpire().toString());
    }
}

