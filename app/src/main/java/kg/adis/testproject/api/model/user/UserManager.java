package kg.adis.testproject.api.model.user;

import android.content.Context;

import java.util.HashMap;

import io.reactivex.Observable;

import kg.adis.testproject.ApiManager;
import kg.adis.testproject.api.model.AppResponse;
import retrofit2.Retrofit;

public class UserManager extends ApiManager {
    private static UserManager instance;
    private UserApiService service;


    private UserManager() {
        Retrofit retrofit = super.buildRetrofitService();
        service = retrofit.create(UserApiService.class);
    }

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }

        return instance;
    }

    public Observable<AppResponse> pre_register(String email, String username) {
        HashMap<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("username", username);
        return service.pre_register(map);
    }

    public Observable<AppResponse> activate(String email, String otp) {
        HashMap<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("otp", otp);
        return service.activate(map);
    }



    public Observable<AppResponse> login(String email, String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("password", password);
        return service.login(map);
    }




    public Observable<AppResponse> register(String email, String username, String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put("email", email);
        map.put("username", username);
        map.put("password", password);
        return service.register(map);
    }

    public Observable<AppResponse> logout(Context context) {
        HashMap<String, String> map = new HashMap<>();
        map.put("access_token", User.getInstance(context).getToken());
        return service.logout(map);
    }

    public Observable<AppResponse> updateUserPassword(Context context, String currentPassword, String newPassword) {
        HashMap<String, String> map = new HashMap<>();

        map.put("access_token", User.getInstance(context).getToken());
        map.put("old_password", currentPassword);
        map.put("new_password", newPassword);

        return service.updateUserPassword(map);
    }
}
