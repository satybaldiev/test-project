package kg.adis.testproject.api.model;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class AppResponse {
    private int status;
    private String message;
    @Nullable
    private JsonElement data;

    public AppResponse() {
        this.status = 0;
        this.message = "";
        this.data = null;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public boolean OK() {
        return this.getStatus() == 0;
    }

    public boolean isTokenExpired() {
        return this.getStatus() == 1400;
    }
}
