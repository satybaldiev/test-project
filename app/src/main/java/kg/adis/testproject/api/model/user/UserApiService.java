package kg.adis.testproject.api.model.user;

import java.util.HashMap;

import io.reactivex.Observable;
import kg.adis.testproject.api.model.AppResponse;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserApiService {

    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("register")
    Observable<AppResponse> register(@FieldMap HashMap<String, String> map);


    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("pre_register")
    Observable<AppResponse> pre_register(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("activate")
    Observable<AppResponse> activate(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("login")
    Observable<AppResponse> login(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("logout")
    Observable<AppResponse> logout(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @Headers({"Accept: application/json"})
    @POST("update/password")
    Observable<AppResponse> updateUserPassword(@FieldMap HashMap<String, String> map);
}
