package kg.adis.testproject.api.model.user;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.AppResponse;
import kg.adis.testproject.util.NetworkUtil;
import kg.adis.testproject.util.RxUtils;


public class UserService {

    private static final String TAG = "UserServiceTag";

    private static Disposable mDisposable;


    public static void logoutUser(final Context context) {

        if (!NetworkUtil.isNetworkConnected(context)) {
            Toast toast = Toast.makeText(context,
                    R.string.check_internet,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else if (User.getInstance(context).isTokenExpired()) {
            Toast toast = Toast.makeText(context,
                    R.string.token_expired,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            RxUtils.unsubscribe(mDisposable);

            mDisposable = UserManager.getInstance().logout(context)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<AppResponse>() {
                        @Override
                        public void onNext(AppResponse response) {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());
                            NetworkUtil.checkHttpStatus(context, e);
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

    public static void updateUserPassword(final Context context, String currentPassword, String newPassword) {

        if (!NetworkUtil.isNetworkConnected(context)) {
            Toast toast = Toast.makeText(context,
                    R.string.check_internet,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else if (User.getInstance(context).isTokenExpired()) {
            Toast toast = Toast.makeText(context,
                    R.string.token_expired,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            RxUtils.unsubscribe(mDisposable);

            mDisposable = UserManager.getInstance().updateUserPassword(context, currentPassword, newPassword)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<AppResponse>() {
                        @Override
                        public void onNext(AppResponse response) {
                            if (response.OK()) {
                                User.getInstance(context).clearApiDate();
                            }

                            Toast toast = Toast.makeText(context,
                                    response.getMessage(),
                                    Toast.LENGTH_SHORT);
                            toast.show();
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());

                            NetworkUtil.checkHttpStatus(context, e);
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }


    public static void unsubscribe() {
        RxUtils.unsubscribe(mDisposable);
    }
}
