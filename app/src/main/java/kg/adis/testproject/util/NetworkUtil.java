package kg.adis.testproject.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import kg.adis.testproject.R;


public class NetworkUtil {
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private static boolean isHttpStatusCode(Throwable throwable, int statusCode) {
        return throwable instanceof retrofit2.HttpException
                && ((retrofit2.HttpException) throwable).code() == statusCode;
    }

    public static void checkHttpStatus(Context context, Throwable throwable) {
        if(isHttpStatusCode(throwable, 404)) {

            Toast toast = Toast.makeText(context,
                    R.string.url_not_found,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Toast toast = Toast.makeText(context,
                    R.string.server_error,
                    Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
