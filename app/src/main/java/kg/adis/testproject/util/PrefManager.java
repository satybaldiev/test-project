package kg.adis.testproject.util;

import android.content.Context;
import android.content.SharedPreferences;

import static kg.adis.testproject.AppConfig.EMAIL_SHARED;
import static kg.adis.testproject.AppConfig.IS_FIRST_TIME_LAUNCH;
import static kg.adis.testproject.AppConfig.PREF_NAME;
import static kg.adis.testproject.AppConfig.USERNAME_SHARED;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public void setTempCredentials(String username, String email) {
        editor.putString(USERNAME_SHARED, username);
        editor.putString(EMAIL_SHARED, email);
        editor.commit();
    }

    public String getTempUsername() {
        return pref.getString(USERNAME_SHARED, "");
    }

    public String getTempEmail() {
        return pref.getString(EMAIL_SHARED, "");
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
