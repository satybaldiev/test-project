package kg.adis.testproject.ui.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.AppResponse;
import kg.adis.testproject.api.model.user.User;
import kg.adis.testproject.api.model.user.UserManager;
import kg.adis.testproject.util.NetworkUtil;
import kg.adis.testproject.util.RxUtils;

public class LoginActivity extends AppCompatActivity {


    static final String TAG = "LoginActivityTag";

    Disposable mDisposable;

    Context mContext;

    @BindView(R.id.email)
    EditText mEmail;

    @BindView(R.id.password)
    EditText mPassword;

    @BindView(R.id.btn_login)
    Button mLoginBtn;

    @BindView(R.id.btn_signup)
    TextView mSignUp;

    @BindView(R.id.btn_forgot_password)
    TextView mForgotPassword;

    @BindView(R.id.email_error_message)
    TextView mErrorMessagePlaceEmail;

    @BindView(R.id.credential_error_message)
    TextView mErrorMessagePlaceCredentials;
    @BindView(R.id.password_error_message)
    TextView mErrorMessagePlacePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mContext = LoginActivity.this;
        resetErrors();

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetErrors();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                // Check for a valid email address.
                if (TextUtils.isEmpty(email) || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mErrorMessagePlaceEmail.setVisibility(View.VISIBLE);
                    mEmail.requestFocus();

                    GradientDrawable border = (GradientDrawable) mEmail.getBackground();
                    border.setStroke(1, getResources().getColor(R.color.colorErrors));
                } else if (TextUtils.isEmpty(password)) {
                    mPassword.requestFocus();
                    GradientDrawable border = (GradientDrawable) mPassword.getBackground();
                    border.setStroke(1, getResources().getColor(R.color.colorErrors));

                } else {
                    performLogin(mContext,email,password);

                }
            }
        });

        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signupActivity = new Intent(mContext, RegisterActivity.class);
                finish();

                startActivity(signupActivity);
            }
        });

        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signupActivity = new Intent(mContext, MainActivity.class);
                finish();
                startActivity(signupActivity);
            }
        });
    }

    private void resetErrors() {
        mErrorMessagePlaceEmail.setVisibility(View.GONE);
        mErrorMessagePlacePassword.setVisibility(View.GONE);
        mErrorMessagePlaceCredentials.setVisibility(View.GONE);


        GradientDrawable password_border = (GradientDrawable) mPassword.getBackground();
        password_border.setStroke(1, getResources().getColor(R.color.borderColor));

        GradientDrawable email_border = (GradientDrawable) mEmail.getBackground();
        email_border.setStroke(1, getResources().getColor(R.color.borderColor));

    }

    private void performLogin(Context context, String email, String password) {

        if (!NetworkUtil.isNetworkConnected(context)) {

            Toast toast = Toast.makeText(context,
                    R.string.check_internet,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            RxUtils.unsubscribe(mDisposable);
            mDisposable = UserManager.getInstance().login(email,password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<AppResponse>() {
                        @Override
                        public void onNext(AppResponse response) {
                            if (response.OK()) {
                                User.getInstance(mContext).update(response.getData());

                                Intent intent = new Intent(mContext, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(intent);

                                finish();
                            } else {
                                mPassword.setText("");
                                mPassword.requestFocus();

                                mErrorMessagePlaceCredentials.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());

                            NetworkUtil.checkHttpStatus(mContext, e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }


}
