package kg.adis.testproject.ui.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.AppResponse;
import kg.adis.testproject.api.model.user.UserManager;
import kg.adis.testproject.util.NetworkUtil;
import kg.adis.testproject.util.PrefManager;
import kg.adis.testproject.util.RxUtils;

public class ActivationActivity extends AppCompatActivity {

    static final String TAG = "ActivationActivityTag";

    Disposable mDisposable;
    Context mContext;
    String mEmail;
    String mUsername;

    @BindView(R.id.otp_password)
    EditText mOtpPassword;

    @BindView(R.id.btn_activate)
    Button mActivateBtn;

    @BindView(R.id.otp_error_message)
    TextView mErrorMessagePlaceOrp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activation);
        ButterKnife.bind(this);
        mContext = ActivationActivity.this;


        PrefManager prefManager = new PrefManager(mContext);

        mUsername = prefManager.getTempUsername();
        mEmail = prefManager.getTempEmail();

        mActivateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetErrors();
                String password = mOtpPassword.getText().toString();

                RxUtils.unsubscribe(mDisposable);
                mDisposable = UserManager.getInstance().activate(mEmail,password)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableObserver<AppResponse>() {
                            @Override
                            public void onNext(AppResponse response) {
                                if (response.OK()) {
                                    Intent newPasswordActivity = new Intent(mContext, NewPasswordActivity.class);
                                    finish();
                                    startActivity(newPasswordActivity);

                                } else {
                                    String message = response.getMessage();
                                    mErrorMessagePlaceOrp.setVisibility(View.VISIBLE);
                                    mErrorMessagePlaceOrp.setText(message);
                                    mOtpPassword.requestFocus();
                                    GradientDrawable border = (GradientDrawable) mOtpPassword.getBackground();
                                    border.setStroke(1, getResources().getColor(R.color.colorErrors));
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, e.getMessage());

                                NetworkUtil.checkHttpStatus(mContext, e);
                            }

                            @Override
                            public void onComplete() {
                            }
                        });


            }
        });

    }

    private void resetErrors() {
        mErrorMessagePlaceOrp.setVisibility(View.GONE);
        GradientDrawable password_border = (GradientDrawable) mOtpPassword.getBackground();
        password_border.setStroke(1, getResources().getColor(R.color.borderColor));

    }
}
