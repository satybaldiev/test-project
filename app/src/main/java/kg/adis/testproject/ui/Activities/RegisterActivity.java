package kg.adis.testproject.ui.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.AppResponse;
import kg.adis.testproject.api.model.user.User;
import kg.adis.testproject.api.model.user.UserManager;
import kg.adis.testproject.util.NetworkUtil;
import kg.adis.testproject.util.PrefManager;
import kg.adis.testproject.util.RxUtils;

public class RegisterActivity extends AppCompatActivity {

    static final String TAG = "RegisterActivityTag";

    Context mContext;
    Boolean validEmail = false;
    Boolean validUsername = false;
    Boolean termAccepted = false;
    Disposable mDisposable;


    @BindView(R.id.email)
    EditText mEmail;

    @BindView(R.id.username)
    EditText mUsername;

    @BindView(R.id.btn_signup)
    Button mSignUp;

    @BindView(R.id.btn_login)
    TextView mLogin;

    @BindView(R.id.terms)
    CheckBox mTermsOfUse;

    @BindView(R.id.email_error_message)
    TextView mErrorMessagePlaceEmail;

    @BindView(R.id.username_error_message)
    TextView mErrorMessagePlaceUsername;

    @BindView(R.id.term_error_message)
    TextView mErrorMessagePlaceTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        mContext = RegisterActivity.this;
        resetErrors();

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginActivity = new Intent(mContext, LoginActivity.class);
                finish();
                startActivity(loginActivity);
            }
        });

        mTermsOfUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                termAccepted = isChecked;
                if (termAccepted) {
                    mErrorMessagePlaceTerms.setVisibility(View.GONE);
                }
            }
        });

        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_username();
                check_email();
                if (validUsername && validEmail) {
                    if (termAccepted) {
                        String username = mUsername.getText().toString();
                        String email = mEmail.getText().toString();
                        performRegister(mContext,username,email);
                    } else {
                        set_error_terms();
                    }
                }
            }
        });


    }

    private void performRegister(final Context context, final String username, final String email) {

        if (!NetworkUtil.isNetworkConnected(context)) {
            Toast toast = Toast.makeText(context,
                    R.string.check_internet,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            RxUtils.unsubscribe(mDisposable);
            mDisposable = UserManager.getInstance().pre_register(email,username)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<AppResponse>() {
                        @Override
                        public void onNext(AppResponse response) {
                            if (response.OK()) {
                                Intent intent = new Intent(mContext, ActivationActivity.class);
                                //Session ???
                                PrefManager prefManager = new PrefManager(mContext);
                                prefManager.setTempCredentials(username,email);


                                startActivity(intent);
                                finish();
                            } else {

                                try {
                                    JSONObject message = new JSONObject(response.getMessage());
                                    if (message.has("username"))
                                    {
                                        mErrorMessagePlaceUsername.setText(message.getJSONArray("username").get(0).toString());
                                        set_error_username();
                                    }
                                    if (message.has("email"))
                                    {
                                        mErrorMessagePlaceEmail.setText(message.getJSONArray("email").get(0).toString());
                                        set_error_email();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());

                            NetworkUtil.checkHttpStatus(mContext, e);
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

    private void check_username() {
        String username = mUsername.getText().toString();

        if (TextUtils.isEmpty(username)) {
            set_error_username(R.string.enter_username);
        } else {
            mUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0);
            mErrorMessagePlaceUsername.setVisibility(View.GONE);
            GradientDrawable border = (GradientDrawable) mUsername.getBackground();
            border.setStroke(1, getResources().getColor(R.color.borderColor));
            validUsername = true;
        }
    }

    private void check_email() {
        String email = mEmail.getText().toString();

        if (email.isEmpty()) {
            set_error_email(R.string.enter_email);
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            set_error_email(R.string.enter_valid_email);
        } else {
            mEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0);
            validEmail = true;
            GradientDrawable border = (GradientDrawable) mEmail.getBackground();
            border.setStroke(1, getResources().getColor(R.color.borderColor));
            mErrorMessagePlaceEmail.setVisibility(View.GONE);
        }

    }


    private void set_error_terms() {
        mErrorMessagePlaceTerms.setText(R.string.accept_term);
        mErrorMessagePlaceTerms.setVisibility(View.VISIBLE);
    }

    private void set_error_username(final int message) {
        mUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlaceUsername.setText(message);
        mErrorMessagePlaceUsername.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mUsername.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validUsername = false;
    }

    private void set_error_username() {
        mUsername.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlaceUsername.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mUsername.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validUsername = false;
    }

    private void set_error_email(final int message) {
        mEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlaceEmail.setText(message);
        mErrorMessagePlaceEmail.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mEmail.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validEmail = false;
    }


    private void set_error_email()
    {
        mEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlaceEmail.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mEmail.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validEmail = false;
    }


    private void resetErrors() {
        mErrorMessagePlaceEmail.setVisibility(View.GONE);
        mErrorMessagePlaceUsername.setVisibility(View.GONE);
        mErrorMessagePlaceTerms.setVisibility(View.GONE);

        GradientDrawable username_border = (GradientDrawable) mUsername.getBackground();
        username_border.setStroke(1, getResources().getColor(R.color.borderColor));

        GradientDrawable email_border = (GradientDrawable) mEmail.getBackground();
        email_border.setStroke(1, getResources().getColor(R.color.borderColor));
    }
}
