package kg.adis.testproject.ui.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.AppResponse;
import kg.adis.testproject.api.model.user.User;
import kg.adis.testproject.api.model.user.UserManager;
import kg.adis.testproject.util.NetworkUtil;
import kg.adis.testproject.util.PrefManager;
import kg.adis.testproject.util.RxUtils;

public class NewPasswordActivity extends AppCompatActivity {

    static final String TAG = "NewPasswordActivityTag";

    Context mContext;
    Disposable mDisposable;
    boolean validPasswordConfirmation;
    boolean validPassword;
    String mUsername;
    String mEmail;


    @BindView(R.id.password)
    EditText mPassword;

    @BindView(R.id.password_confirmation)
    EditText mPasswordConfirmation;

    @BindView(R.id.btn_submit)
    Button mSubmit;

    @BindView(R.id.password_error_message)
    TextView mErrorMessagePlacePassword;

    @BindView(R.id.password_confirmation_error_message)
    TextView mErrorMessagePlacePasswordConfirmation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        ButterKnife.bind(this);
        mContext = NewPasswordActivity.this;
        PrefManager prefManager = new PrefManager(mContext);
        mUsername = prefManager.getTempUsername();
        mEmail = prefManager.getTempEmail();
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check_password();
                check_password_confirmation();
                if (validPassword && validPasswordConfirmation) {
                    String password = mPassword.getText().toString();
                    performRegister(mContext,mEmail,mUsername,password);
                }
            }
        });
    }

    private void check_password() {
        String password = mPassword.getText().toString();

        if (password.isEmpty()) {
            set_error_password(R.string.enter_password);
        } else if (password.length() < 6) {
            set_error_password(R.string.enter_password_min);
        } else {
            mPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0);
            validPassword = true;
            GradientDrawable border = (GradientDrawable) mPassword.getBackground();
            border.setStroke(1, getResources().getColor(R.color.borderColor));
            mErrorMessagePlacePassword.setVisibility(View.GONE);
        }
    }

    private void check_password_confirmation() {
        String password = mPassword.getText().toString();
        String password_confirmation = mPasswordConfirmation.getText().toString();

        if (password_confirmation.isEmpty()) {
            set_error_password_confirmation(R.string.enter_password);
        } else if (!password_confirmation.equals(password)) {
            set_error_password_confirmation(R.string.password_confirmation_error);
        } else {
            mPasswordConfirmation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_green_24dp, 0);
            validPasswordConfirmation = true;
            GradientDrawable border = (GradientDrawable) mPasswordConfirmation.getBackground();
            border.setStroke(1, getResources().getColor(R.color.borderColor));
            mErrorMessagePlacePasswordConfirmation.setVisibility(View.GONE);
        }
    }

    private void set_error_password(final int message) {
        mPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlacePassword.setText(message);
        mErrorMessagePlacePassword.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mPassword.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validPassword = false;
    }

    private void set_error_password_confirmation(final int message) {
        mPasswordConfirmation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_error_red_24dp, 0);
        mErrorMessagePlacePasswordConfirmation.setText(message);
        mErrorMessagePlacePasswordConfirmation.setVisibility(View.VISIBLE);
        GradientDrawable border = (GradientDrawable) mPasswordConfirmation.getBackground();
        border.setStroke(1, getResources().getColor(R.color.colorErrors));
        validPasswordConfirmation = false;
    }

    private void performRegister(Context context, String email, String username, String password) {

        if (!NetworkUtil.isNetworkConnected(context)) {
            Toast toast = Toast.makeText(context,
                    R.string.check_internet,
                    Toast.LENGTH_SHORT);
            toast.show();
        } else {
            RxUtils.unsubscribe(mDisposable);


            mDisposable = UserManager.getInstance().register(email,username,password)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<AppResponse>() {
                        @Override
                        public void onNext(AppResponse response) {
                            if (response.OK()) {
                                User.getInstance(mContext).update(response.getData());
                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                mPassword.setText("");
                                mPasswordConfirmation.setText("");

                                Toast toast = Toast.makeText(mContext,
                                        response.getMessage(),
                                        Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, e.getMessage());

                            NetworkUtil.checkHttpStatus(mContext, e);

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }

}
