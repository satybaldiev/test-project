package kg.adis.testproject.ui.Activities;


import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import kg.adis.testproject.R;
import kg.adis.testproject.api.model.user.User;
import kg.adis.testproject.api.model.user.UserService;
import kg.adis.testproject.ui.Fragments.GiftpayFragment;
import kg.adis.testproject.ui.Fragments.MoreFragment;
import kg.adis.testproject.ui.Fragments.SettingsFragment;
import kg.adis.testproject.ui.Fragments.TransactionsFragment;
import kg.adis.testproject.ui.Fragments.WithdrawalFragment;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "MainActivityTag";

    Context mContext;
    User mUser;

    String mUsername;
    String mToken;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView mBottomNavigationView;
    boolean mDoubleBackToExit = false;


    @Override
    protected void onPostResume() {
        super.onPostResume();

        isLoginNeeded(mContext);
        mUsername = mUser.getUserame();
        mToken = mUser.getToken();
    }


    private void isLoginNeeded(Context context) {
        if (mUser.isTokenExpired() || mUser.getUserame().isEmpty() || mUser.getToken().isEmpty()) {
            Intent intent = new Intent(context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mContext = MainActivity.this;
        mUser = User.getInstance(mContext);

        mContext = MainActivity.this;
        loadFragment(new SettingsFragment());
        mBottomNavigationView.setSelectedItemId(R.id.action_settings);

        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;

                switch (item.getItemId()) {
                    case R.id.action_transactions:
                        fragment = new TransactionsFragment();
                        break;
                    case R.id.action_withdrawl:
                        fragment = new WithdrawalFragment();
                        break;
                    case R.id.action_giftpay:
                        fragment = new GiftpayFragment();
                        break;
                    case R.id.action_settings:
                        fragment = new SettingsFragment();
                        break;
                    case R.id.action_more:
                        //fragment = new MoreFragment();
                        //for test
                        UserService.logoutUser(mContext);
                        User.getInstance(mContext).clear_user_credentials();
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        finish();
                        startActivity(intent);
                        break;
                }
                return loadFragment(fragment);
            }
        });
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserService.unsubscribe();
    }


}
